    <?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = "No encontrado (#404)";//$name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?//= nl2br(Html::encode($message)) ?>
        La página que estás buscando no está disponible o no existe (#404)
    </div>

    <p>     
    El error anterior ocurrió mientras el servidor web estaba procesando su solicitud.
    </p>
    <p>
    Póngase en contacto con nosotros si cree que se trata de un error del servidor. Gracias.
    </p>

</div>
