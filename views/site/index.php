<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'GyGAbogados';

//var_dump($noticias);
$index=0;
$cont=0;
$classActive="active";
?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOS9NPYdzKctvcfB5gp6u7cwBd8X-Qzt8&callback=initMap"
    async defer></script>
<body>
	<div class="">

      <div class="site-wrapper-inner">

        <div class="cover-container">

        	<div class="masthead clearfix">
          <div>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

	              <!-- INDICATORS -->
	              <ol class="carousel-indicators">
	              <?php foreach ($slider as $key => $value) { ?>
	              <?php if ($cont==0){ $styleActive=$classActive; }else{ $styleActive="";  } ?>

	              		<li data-target="#myCarousel" data-slide-to="<?=$cont?>" class="<?=$styleActive?>"></li>
	              <?php $cont++ ?>
	              <?php } ?>
	              </ol>

	              <!-- Wrapper for slides -->
	              <div class="carousel-inner">
	              	<?php $cont=0; ?>
	                <?php foreach ($slider as $key => $value) { $cont++; ?>
	                <?php if ($cont==1){ $styleActive=$classActive; }else{ $styleActive="";  } ?>
	              		<div class="item <?=$styleActive?>">
		                  <img src="<?= URL::base() ?>/images/banners/<?=$value->image?>" alt="<?=$value->alt?>">
		                </div>
	                <?php } ?>
	              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
              </a>

            
            </div>
          </div>
          	    <!-- Sección Videos -->

                <div class="servicios">
                  <div class="content-servicios">
                    <div class="">
                      <img src="<?= URL::base() ?>/images/videos.png" alt="Videos">
                      
                      <div class="dv-servicios" style="margin-top: 10px;background-color: black;">
                        <div class="videosHome left">
                          <iframe width="100%" height="500" src="https://www.youtube.com/embed/TeBUWpMhT9M" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="videosHome right">
                          <div class="videoItem">
                            <img src="http://img.youtube.com/vi/CQd3JUUp4JY/0.jpg" alt="" class="video-thumbk">
                            <span>Menorca & Casa Bonita</span>
                          </div>
                            <div class="videoItem">
                              <img src="http://img.youtube.com/vi/TeBUWpMhT9M/0.jpg" alt="" class="video-thumbk">
                              <span>Babelatino Radio Web</span>
                            </div>
                            <div class="videoItem">
                              <img src="http://img.youtube.com/vi/CQd3JUUp4JY/0.jpg" alt="" class="video-thumbk">
                              <span>Menorca & Casa Bonita</span>
                            </div>
                            <div class="videoItem">
                              <img src="http://img.youtube.com/vi/TeBUWpMhT9M/0.jpg" alt="" class="video-thumbk">
                              <span>Babelatino Radio Web</span>
                            </div>
                        </div>
                      	<?php $cont=0; ?>      
	            		 

                        <!--<div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_consultoria.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_representacion.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_patrocinio.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>-->
                      </div>
                      <div style="margin-top: 15px;">
                        <input type="button" class="boton" name="enviar" value="VER MÁS" style=""><br>&nbsp;
                      </div>
                    </div>
                     <div class="line-sep"></div>
                  </div>

                </div>
          	    <!-- Fin Sección Videos -->
          	    
          	    <!-- Sección Colaboradores -->
	      	    <div class="colaboradores">
	      	    	<div class="content-colaboradores">
	      	    		 <img src="<?= URL::base() ?>/images/programas.png" alt="Videos">
	      	    		<div class="">
	      	    			<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	      	    		</div>
	      	    		<?php $cont=0; ?>      
	            		<?php foreach ($colaboradores as $key => $value) { $cont++; ?>
	      	    			<div class="dv-colaboradorint">
	      	    				<div class="img-colabordor">
	      	    					<img src="<?= URL::base() ?>/images/colaboradores/<?=$value->imagen?>" class="">
	      	    				</div>
	      	    				<div class="txt-colaborador">
	      	    					<span class="nombre-col"><?=$value->nombres?></span><br>
	      	    					<span class="titulo-col"><?=$value->titulo?></span><br>
	      	    					<span class="descripcion-col"><?=$value->descripcion ?></span>
	      	    				</div>
	      	    			</div>
	      	    			<!--<div class="dv-colaboradorint">
	      	    				<div class="img-colabordor">
	      	    					<img src="<?//= URL::base() ?>/images/colaboradores/abogado_other.png" class="">
	      	    				</div>
	      	    				<div class="txt-colaborador">
	      	    					<span class="nombre-col">KLEBER GONZALEZ</span><br>
	      	    					<span class="titulo-col">Abogado</span><br>
	      	    					<span class="descripcion-col">Abogado con gran experiencia en asuntos penales</span>
	      	    				</div>
	      	    			</div>-->
	      	    		<?php } ?>
	      	    	
                </div>
                <div class="line-sep"></div>
	      	   	</div>

          	    <!-- Fin Sección Colaboradores -->

              
                   <!-- Sección Eventos -->
              <div class="servicios">
                  <div class="content-servicios" style="padding-top: 0%; ">
                    <div class="">
                      <img src="<?= URL::base() ?>/images/eventos.png" alt="Eventos">
                      
                      <div class="dv-servicios" style="margin-top:10px;background-color: transparent;    border: 2px solid #661106;">
                        <div class="eventosHome left">
                          <img style="width: 100%;" src="<?= URL::base() ?>/images/eventos/evento1.png" alt="Eventos" >
                        </div>
                        <div class="eventosHome right" style="padding-bottom: 0px; padding-right: 1%;">
                          <div>
                            <span>&nbsp;</span>
                            <img src="<?= URL::base() ?>/images/logo.png" alt="logo" class="logo" style="margin-top: 0px;">
                          </div>
                          <div style="text-align: left; padding-top: 10px; padding-left: 2%; line-height: 30px;">
                            <br>
                            <div style="text-align: center;">
                              <span style="font-weight: bold; font-size: 12px;">EVENTO:</span>
                              <span style="font-weight: normal; font-size: 12px; color: #858594;">PROYECCIÓN DEL DOCUMENTAL “ALFARO VIVE CARAJO</span><br><br>
                            </div>
                            <span style="font-weight: bold; font-size: 12px;">FECHA:</span>
                            <span style="font-weight: normal; font-size: 12px; color: #858594;">VIERNES 01 DE JULIO DEL 2017</span><br>
                            <span style="font-weight: bold; font-size: 12px;">HORA:</span>
                            <span style="font-weight: normal; font-size: 12px; color: #858594;">19:00</span><br>
                            <span style="font-weight: bold; font-size: 12px;">LUGAR:</span>
                            <span style="font-weight: normal; font-size: 12px; color: #858594;">PLAZA DE LOS COMUNES-PLAZA PEÚELA 3, M EMBAJADORES</span><br>
                            <div style="text-align: center;">
                              <span style="font-weight: bold; font-size: 12px;">MAPA:</span><br>
                              <div id="map" style="height: 150px;"></div>
                            </div>
                          </div>
                        </div>
                        <?php $cont=0; ?>      
                   

                        <!--<div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_consultoria.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_representacion.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_patrocinio.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>-->
                      </div>
                      <div style="margin-top: 15px;">
                        <input type="button" class="boton" name="enviar" value="VER MÁS" style=""><br>&nbsp;
                      </div>
                    </div>
                     <div class="line-sep"></div>
                     <br><br>
                  </div>

                </div>
                <!-- Fin Sección Eventos -->


                  </div>

                </div>
            </div>    
  <script type="text/javascript">
      function initMap() {
        var myLatLng = {lat: 40.400966, lng:-3.7028751};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Plaza de los comunes'
        });

      }
  </script>

</body>
</html>