<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Porque nos da la gana';
?>
<section class="site-registro">

	<div >

		<div class="jumbotron">
			<div class="column-left">
				<img src="<?= URL::base() ?>/images/colaboradores/patriciasosa.png" class="back-seccion">
				<div class="txt-colaborador"><br>
					<span class="nombre-col">PATRICIA SOSA</span><br>
					<span class="titulo-col"></span><br>
					<span class="descripcion-col"><?=$this->title?></span>
				</div>
			</div>
			<div class="column-right">
				<div class="seccion-text"> <br>Estamos aquí para continuar con nuestra nueva versión de BABELATINO WEB, un nuevo espacio revonado lleno de emociones y ganas de continuar con este reto que lo inauguramos hace más de 8 años y que pese a las dificultades y tropiezos para mantener nuestra esencia intercultural Babelatino, hemos logrado sostenerla y hacerla crecer juntos. Ahora con más fuerza y ganas de seguir dando nuestras voces radiales, nuestro único sueño es contribuir a construir un mundo sin fronteras, donde podamos compartir justicia y humanidad.<br><br>
	Queremos acercar nuestra Gran Patria Latinoamericana al mundo y en especial generar una fuente de comunicación abierta interactiva y participativa, consolidar y abrir una plataforma donde todos pueden sentirse participes de una única voz, uniendo música, comunicación, fotografía, literatura y más 
	Del Grupo Babelatino somos parte un puñado de compañeros con calidad profesional y en especial profunda humanidad, unificando esfuerzos e iniciativas en comunicación libre superamos los límites y egos individuales para construir juntos un proyecto pionero en interactividad 
	Les invitamos a ser parte de nuestra red, que es de ustedes, que son la razón de nuestra existencia. </div>
				</div>
			</div>

		<div class="body-content">
nhhh


		</div>
	</div>
</section>
<style type="text/css">
.txt-colaborador .nombre-col 
{
    color: #CC0000;
    font-size: 14px;
}
.txt-colaborador .descripcion-col {
    color: black;
    font-size: 18px;
    text-transform: uppercase;
}
.jumbotron .seccion-text {
    color: black;
    font-size: 12px;
    font-style: oblique;
    text-shadow: none;
}
.jumbotron .column-left {
    display: inline-block;
    width: 30%;
    vertical-align: middle;
}

.jumbotron .column-right {
    display: inline-block;
    width: 69%;
    vertical-align: middle;
}

</style>
