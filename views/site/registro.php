<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Registro';
?>
<section class="site-registro">

    <div >

        <div class="jumbotron">
            <h1><?=$this->title?>!</h1>

            <p><a class="btn btn-lg btn-success" href="/web/site/ticket">SIGUIENTE</a></p>
        </div>

        <div class="body-content">



        </div>
    </div>
</section>
