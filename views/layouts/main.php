<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs; 
use app\models\Menu;
use app\models\Configuraciones;


$classActive="active";
$cssStyle="";
//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= URL::base() ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?= URL::base() ?>/css/site.css" rel="stylesheet" media="screen">
    <link href="<?= URL::base() ?>/css/carousel.css" rel="stylesheet" media="screen">
    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="<?= URL::base() ?>/js/bootstrap.min.js"></script>

  <title>Babelatino</title>
</head>
<!DOCTYPE html>

<?php $this->beginBody() ?>


<div class="inner">
  <h3 class="masthead-brand"><img src="<?= URL::base() ?>/images/logo.png" alt="logo" class="logo"></h3>
  <nav>
    <ul class="nav masthead-nav">
      <li class="home-nav"><a href="<?= URL::base() ?>/"><img src="<?= URL::base() ?>/images/home.png" alt="home"></a></li>

      <?php $menus= Menu::find()->where(["estatus"=>"ACTIVO"])->orderBy(['orden'=>SORT_ASC])->all(); ?>
      <?php $pageActual=Yii::$app->controller->action->id; ?> 
      <?php if ($pageActual=="index"){ $pageActual="inicio"; }else{ $pageActual=Yii::$app->controller->action->id;  } ?>
      <?php if ($pageActual=="login"){ $pageActual="micuenta"; }?>
      <?php if ($pageActual=="contactanos"){ $pageActual="contáctanos"; }?>

          <?php foreach ($menus as $key => $value) { ?>
          <?php $descripcionMenu=$value->menu; ?>
          <?php $descripcionMenu=strtolower($descripcionMenu); ?>
          <?php $descripcionMenu=str_replace(" ", "", $descripcionMenu); ?>
          <?php if ($descripcionMenu==$pageActual){ $cssStyle=$classActive; }else{ $cssStyle=""; } ?>
          <?php if ($descripcionMenu=="inicio"){ $url=''; }else{ $url='/site/'; } ?>
          <li class="<?=$cssStyle?>"><a href="<?= URL::base() ?><?=$url?><?=$value->link?>"><?=$value->menu?></a></li>
          <?php
      }
      ?> 
  </ul>
</nav>
</div>


<?= $content ?>


<?php $configuraciones= Configuraciones::find()->where(["estatus"=>"ACTIVO"])->all(); ?>
<?php $direccion=""; $horario=""; $correo=""; $telefonos=""; $facebook=""; $twitter=""; $instagram=""; ?>
<?php foreach ($configuraciones as $key => $value) {
    if ($value->tipo=="FOOTER_DIRECCION"){ $direccion=$value->contenido; }
    if ($value->tipo=="FOOTER_HORARIO"){ $horario=$value->contenido; }
    if ($value->tipo=="FOOTER_CORREO"){ $correo=$value->contenido; }
    if ($value->tipo=="FOOTER_TELEFONOS"){ $telefonos=$value->contenido; }
    if ($value->tipo=="FOOTER_FACEBOOK"){ $facebook=$value->contenido; }
    if ($value->tipo=="FOOTER_TWITTER"){ $twitter=$value->contenido; }
    if ($value->tipo=="FOOTER_INSTAGRAM"){ $instagram=$value->contenido; }
} ?>


<div class="footer">

    <div class="content-footer">
      <div class="" style="padding-bottom: 10px;">
        <img src="<?= URL::base() ?>/images/facebook.fw.png">&nbsp;&nbsp;
        <img src="<?= URL::base() ?>/images/twitter.fw.png">&nbsp;&nbsp;
        <img src="<?= URL::base() ?>/images/instagram.fw.png">
      </div>
      <div style="width: 100%;">
         <img src="<?= URL::base() ?>/images/footer.fw.png" alt="logo" class="logo">
      </div>  
    </div>
</div>

</body>
<?php //$this->endBody() ?>
</html>

<?php $this->endPage() ?>
