/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.25-MariaDB : Database - dbgygabogados
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tblcaracteristicas` */

DROP TABLE IF EXISTS `tblcaracteristicas`;

CREATE TABLE `tblcaracteristicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `icono` varchar(150) DEFAULT NULL,
  `color` varchar(10) NOT NULL,
  `orden` int(3) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tblcaracteristicas` */

insert  into `tblcaracteristicas`(`id`,`titulo`,`descripcion`,`icono`,`color`,`orden`,`fechacreacion`,`estatus`) values (1,'EXPERIENCIA','Somos la firma de Abogados, que cuenta con un equipo de profesionales capacitados en las distintas áreas del derecho','experiencia.png','#000000',NULL,'2017-10-28 17:00:30','ACTIVO'),(2,'CONFIANZAS','Somos la firma de Abogados, que cuenta con un equipo de profesionales capacitados en las distintas áreas del derecho ','confianza.png','#CA9C61',NULL,'2017-10-28 17:01:12','ACTIVO'),(3,'EFECTIVIDAD','Somos la firma de Abogados, que cuenta con un equipo de profesionales capacitados en las distintas áreas del derecho  ','efectividad.png','#000000',NULL,'2017-10-28 17:01:40','ACTIVO');

/*Table structure for table `tblcolaboradores` */

DROP TABLE IF EXISTS `tblcolaboradores`;

CREATE TABLE `tblcolaboradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(200) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(150) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `orden` int(3) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tblcolaboradores` */

insert  into `tblcolaboradores`(`id`,`nombres`,`titulo`,`descripcion`,`imagen`,`telefono`,`mail`,`orden`,`fechacreacion`,`estatus`) values (1,'AB. GEOVANNY MAYORGA ANGOS','SOCIO','Abogado con gran experiencia en varios campos','abogado_giovani.png','(593) 986313781','consultas@gygabogados.ec',1,'2017-10-29 02:34:40','ACTIVO'),(2,'AB. KLEBER GONZALEZ LOQUI','SOCIO','Abogado con gran experiencia en varios campos','abogado_other.png','Tel: (593) 986313781','consultas@gygabogados.ec',2,'2017-10-29 02:35:03','ACTIVO');

/*Table structure for table `tblconfiguraciones` */

DROP TABLE IF EXISTS `tblconfiguraciones`;

CREATE TABLE `tblconfiguraciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` enum('FOOTER_DIRECCION','FOOTER_HORARIO','FOOTER_CORREO','FOOTER_TELEFONOS','FOOTER_FACEBOOK','FOOTER_TWITTER','FOOTER_INSTAGRAM','CORREO_CONSULTA','CONTACTOS_MAPLON','CONTACTOS_MAPLAT') NOT NULL,
  `contenido` varchar(300) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tblconfiguraciones` */

insert  into `tblconfiguraciones`(`id`,`tipo`,`contenido`,`fechacreacion`,`estatus`) values (1,'FOOTER_DIRECCION','Cdla. Urbanor, diagonal a la Gasolinera de la Av. Las Aguas','2017-10-29 11:42:23','ACTIVO'),(2,'FOOTER_HORARIO','Lunes a Viernes 9:00 - 18:00','2017-10-29 11:42:36','ACTIVO'),(3,'FOOTER_CORREO','gglegales@hotmail.com','2017-10-29 11:42:46','ACTIVO'),(4,'FOOTER_TELEFONOS','(593) 0986313786 - (593) 097 943 4399','2017-10-29 11:42:53','ACTIVO');

/*Table structure for table `tblmenu` */

DROP TABLE IF EXISTS `tblmenu`;

CREATE TABLE `tblmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) NOT NULL,
  `link` varchar(400) NOT NULL DEFAULT '#',
  `orden` int(2) DEFAULT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tblmenu` */

insert  into `tblmenu`(`id`,`menu`,`link`,`orden`,`estatus`) values (1,'Inicio','/',1,'ACTIVO'),(2,'Quienes Somos','quienessomos',2,'ACTIVO'),(3,'Servicios','servicios',3,'ACTIVO'),(4,'Noticias','noticias',4,'ACTIVO'),(5,'Contáctanos','contactanos',5,'ACTIVO'),(6,'Mi Cuenta','iniciarsesion',6,'ACTIVO');

/*Table structure for table `tblnoticias` */

DROP TABLE IF EXISTS `tblnoticias`;

CREATE TABLE `tblnoticias` (
  `idnoticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(70) NOT NULL,
  `extracto` varchar(300) NOT NULL,
  `contenido` varchar(900) NOT NULL,
  `home` enum('SI','NO') NOT NULL DEFAULT 'SI',
  `orden` int(11) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `imagen2` varchar(200) DEFAULT NULL,
  `imagen3` varchar(200) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`idnoticia`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tblnoticias` */

insert  into `tblnoticias`(`idnoticia`,`titulo`,`extracto`,`contenido`,`home`,`orden`,`imagen`,`imagen2`,`imagen3`,`video`,`fechacreacion`,`estatus`) values (1,'¿Existe Estado de Necesidad en el Aborto?','De acuerdo a nuestra legislación se ha determinado el Estado de Necesidad, como un estado de la actividad anti-jurídica que debe contener tres elementos constitutivos','De acuerdo a nuestra legislación se ha determinado el Estado de Necesidad, como un estado de la actividad anti-jurídica que debe contener tres elementos constitutivos','SI',1,'noticia_aborto.jpg',NULL,NULL,'','2017-08-15 04:38:46','ACTIVO'),(2,'Indulto','El indulto puede definirse de manera fugaz como una medida de gracia que el poder otorga a los condenados con sentencia firme, levantando la pena que se les hubiera impuesto o parte de ella, y cambiandola por otra más baja','El indulto puede definirse de manera fugaz como una medida de gracia que el poder otorga a los condenados con sentencia firme, levantando la pena que se les hubiera impuesto o parte de ella, y cambiandola por otra más baja','SI',2,'noticia_indulto.jpg',NULL,NULL,NULL,'2017-06-10 11:27:21','ACTIVO'),(3,'Declaración de Parte COGEP','Al leer o escuchar este término, nos trae al imaginario de nuestra mente la figura de un testigo frente al Juez; pues bien, si no les trajo a la mente esta escena entonces les mencionaré que antes del COGEP','Al leer o escuchar este término, nos trae al imaginario de nuestra mente la figura de un testigo frente al Juez; pues bien, si no les trajo a la mente esta escena entonces les mencionaré que antes del COGEP','SI',3,'noticia_declaracion.jpg',NULL,NULL,NULL,'2016-12-07 11:28:20','ACTIVO'),(4,'El Derecho','El derecho, es un término que ha venido siendo la máxima referencia humana sobre su ser, como parte activa en la sociedad, el derecho es un concepto intangible de lo bueno y lo malo tal como el hombre lo percibía desde los inicios de la civilización','El derecho, es un término que ha venido siendo la máxima referencia humana sobre su ser, como parte activa en la sociedad, el derecho es un concepto intangible de lo bueno y lo malo tal como el hombre lo percibía desde los inicios de la civilización','SI',4,'noticia_derecho.jpg',NULL,NULL,NULL,'2016-10-06 11:28:20','ACTIVO');

/*Table structure for table `tblpages` */

DROP TABLE IF EXISTS `tblpages`;

CREATE TABLE `tblpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` enum('QUIENES SOMOS','CONTACTOS','MI CUENTA') NOT NULL,
  `background` varchar(150) NOT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tblpages` */

insert  into `tblpages`(`id`,`seccion`,`background`,`fechacreacion`,`estatus`) values (1,'QUIENES SOMOS','background_quienessomos.jpg','2017-10-29 20:00:35','ACTIVO');

/*Table structure for table `tblservicios` */

DROP TABLE IF EXISTS `tblservicios`;

CREATE TABLE `tblservicios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `home` enum('SI','NO') NOT NULL,
  `imagen` varchar(120) NOT NULL,
  `orden` int(3) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblservicios` */

insert  into `tblservicios`(`id`,`nombre`,`home`,`imagen`,`orden`,`fechacreacion`,`estatus`) values (1,'Defensa','SI','servicio_defensa.jpg',1,'2017-10-28 12:44:49','ACTIVO'),(2,'Consultoria','SI','servicio_consultoria.jpg',2,'2017-10-28 12:45:56','ACTIVO'),(3,'Representación','SI','servicio_representacion.jpg',3,'2017-10-28 12:46:20','ACTIVO'),(4,'Patrocinio de Causa','SI','servicio_patrocinio.jpg',4,'2017-10-28 12:46:53','ACTIVO');

/*Table structure for table `tblslider` */

DROP TABLE IF EXISTS `tblslider`;

CREATE TABLE `tblslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `link` varchar(300) DEFAULT '#',
  `orden` int(3) NOT NULL,
  `alt` varchar(200) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tblslider` */

insert  into `tblslider`(`id`,`titulo`,`image`,`link`,`orden`,`alt`,`fechacreacion`,`estatus`) values (1,'Banner1','banner1.jpg','#',1,NULL,'2017-10-28 12:42:07','ACTIVO'),(2,'Banner2','banner2.jpg','#',2,NULL,'2017-10-28 12:42:15','ACTIVO'),(3,'Banner3','banner3.jpg','#',3,NULL,'2017-10-28 12:42:24','ACTIVO');

/*Table structure for table `tblvisitas` */

DROP TABLE IF EXISTS `tblvisitas`;

CREATE TABLE `tblvisitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnoticia` int(11) NOT NULL,
  `votos` int(11) NOT NULL DEFAULT '0',
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`idnoticia`,`votos`,`fechacreacion`),
  KEY `idnoticia` (`idnoticia`),
  CONSTRAINT `tblvisitas_ibfk_1` FOREIGN KEY (`idnoticia`) REFERENCES `tblnoticias` (`idnoticia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblvisitas` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `identity` varchar(13) NOT NULL,
  `names` varchar(255) NOT NULL,
  `lastnames` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(9) NOT NULL,
  `cellphone` varchar(10) NOT NULL,
  `sex` enum('MALE','FEMALE') NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('CLIENT','ADMIN') NOT NULL DEFAULT 'CLIENT',
  `auth_key` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE',
  `password_reset_token` varchar(255) DEFAULT NULL,
  `sap_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_has_many` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4491 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`city_id`,`identity`,`names`,`lastnames`,`birthday`,`username`,`password`,`phone`,`cellphone`,`sex`,`creation_date`,`type`,`auth_key`,`status`,`password_reset_token`,`sap_id`) values (1,NULL,'0930178462','Mario','Aguilar','2015-12-02','marioaguilar1990@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','2222222','999999999','MALE','2017-07-09 16:28:02','ADMIN','kmfYgLCF3rgdke9HLPMiVTztN5rOylP6','ACTIVE','s9b0OLmv7cc0Ys5PSkOEW1h0-Hu0_5Dk_1452786912',1107181),(4489,NULL,'9999999999','Ricardo','Aules','0000-00-00','y2rick432@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','2222222','999999999','MALE','2017-07-25 04:50:31','ADMIN',NULL,'ACTIVE',NULL,NULL),(4490,NULL,'9999999998','Marco','Reyes','0000-00-00','marco4482@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b','2222222','999999999','MALE','2017-07-25 04:51:28','ADMIN',NULL,'ACTIVE',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
