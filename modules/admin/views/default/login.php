<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Login';

?>
<section id="login" class="background-registro">
    <div class="container-login">
        <div class="cont-titulos">
            <h2>Inicia Sesión</h2>
            <p>Ingresa para poder acceder al administrador </p>
        <!--    <div class="separador-p"><img src="<?//= URL::base() ?>/images/separador.svg"/></div>-->
        </div>
        <div class="cont-formulario">
                <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => ''],
            'fieldConfig' => [ 
                'template' => "<div class=\"cont-campos f-leftc\">{label}{input}{error}</div>",
                   'options' => [
                                'tag'=>'div'

                            ]
                //'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>
           
                <?= $form->field($model, 'username')->input('email')->label('Email') ?>
                <?= $form->field($model, 'password')->passwordInput()->label('Contraseña') ?>
                
            <input type="submit" value="Iniciar Sesión"/>
               <?php ActiveForm::end(); ?>
            

        </div>
    </div>
</section>
<!-- -->
<style type="text/css">
.container
{
    text-align: center;       
}
.container-login
{
    text-align: center;
    width: 50%;
    margin: auto;
    border: 1px solid black;
    padding: 2%;

}
</style>