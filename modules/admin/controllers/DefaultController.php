<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;
use app\models\LoginForm;
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

     public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        //var_dump($model);
        if ($model->load(Yii::$app->request->post()))
        {
            //var_dump($model);
            $model->password=md5($model->password);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            return $this->redirect(['/admin/']);
        } else {
            return $this->render('login', [
                'model' => $model,
                ]);
        }
      
    }
}
