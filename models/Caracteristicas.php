<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblcaracteristicas".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $descripcion
 * @property string $icono
 * @property string $color
 * @property integer $orden
 * @property string $fechacreacion
 * @property string $estatus
 */
class Caracteristicas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblcaracteristicas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion', 'color'], 'required'],
            [['descripcion', 'estatus'], 'string'],
            [['orden'], 'integer'],
            [['fechacreacion'], 'safe'],
            [['titulo'], 'string', 'max' => 100],
            [['icono'], 'string', 'max' => 150],
            [['color'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'icono' => 'Icono',
            'color' => 'Color',
            'orden' => 'Orden',
            'fechacreacion' => 'Fechacreacion',
            'estatus' => 'Estatus',
        ];
    }
}
