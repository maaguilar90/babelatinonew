<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblcolaboradores".
 *
 * @property integer $id
 * @property string $nombres
 * @property string $titulo
 * @property string $descripcion
 * @property string $imagen
 * @property string $telefono
 * @property string $mail
 * @property integer $orden
 * @property string $fechacreacion
 * @property string $estatus
 */
class Colaboradores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblcolaboradores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombres', 'titulo', 'descripcion'], 'required'],
            [['descripcion', 'estatus'], 'string'],
            [['orden'], 'integer'],
            [['fechacreacion'], 'safe'],
            [['nombres'], 'string', 'max' => 200],
            [['titulo', 'mail'], 'string', 'max' => 100],
            [['imagen'], 'string', 'max' => 150],
            [['telefono'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombres' => 'Nombres',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'imagen' => 'Imagen',
            'telefono' => 'Telefono',
            'mail' => 'Mail',
            'orden' => 'Orden',
            'fechacreacion' => 'Fechacreacion',
            'estatus' => 'Estatus',
        ];
    }
}
