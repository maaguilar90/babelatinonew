<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblslider".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $image
 * @property string $link
 * @property integer $orden
 * @property string $alt
 * @property string $fechacreacion
 * @property string $estatus
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblslider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'image', 'orden'], 'required'],
            [['orden'], 'integer'],
            [['fechacreacion'], 'safe'],
            [['estatus'], 'string'],
            [['titulo'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 150],
            [['link'], 'string', 'max' => 300],
            [['alt'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'image' => 'Image',
            'link' => 'Link',
            'orden' => 'Orden',
            'alt' => 'Alt',
            'fechacreacion' => 'Fechacreacion',
            'estatus' => 'Estatus',
        ];
    }
}
