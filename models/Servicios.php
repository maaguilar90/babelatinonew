<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblservicios".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $home
 * @property string $imagen
 * @property integer $orden
 * @property string $fechacreacion
 * @property string $estatus
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblservicios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nombre', 'home', 'imagen'], 'required'],
            [['id', 'orden'], 'integer'],
            [['home', 'estatus'], 'string'],
            [['fechacreacion'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['imagen'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'home' => 'Home',
            'imagen' => 'Imagen',
            'orden' => 'Orden',
            'fechacreacion' => 'Fechacreacion',
            'estatus' => 'Estatus',
        ];
    }
}
