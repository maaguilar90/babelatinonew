<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblmenu".
 *
 * @property integer $id
 * @property string $menu
 * @property string $link
 * @property string $estatus
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblmenu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu'], 'required'],
            [['estatus'], 'string'],
            [['menu'], 'string', 'max' => 50],
            [['link'], 'string', 'max' => 400],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu' => 'Menu',
            'link' => 'Link',
            'estatus' => 'Estatus',
        ];
    }
}
