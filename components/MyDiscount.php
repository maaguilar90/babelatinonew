<?php
namespace app\components;
use yz\shoppingcart\DiscountBehavior;
class MyDiscount extends DiscountBehavior
{
    /**
     * @param CostCalculationEvent $event
     */
    public $value=10;
    public function onCostCalculation($event)
    {
        // Some discount logic, for example
        $event->discountValue = $this->value;
    }
}