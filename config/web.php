<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language'=>'es',
    'timeZone' => 'America/Guayaquil',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '8te1HJt3rRCqy9bQvMN2Ly5Z9f6t1Ouv',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

    ],
    
     'modules' => [
    'gii' => [
      'class' => 'yii\gii\Module', //adding gii module
      'allowedIPs' => ['127.0.0.1', '::1'],  //allowing ip's 
    ],
    'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout'=>'admin',
        ],
  ],
      'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
  // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii']=[
      'class' =>  'yii\gii\Module',
      'allowedIPs' => ['*'],
    ];
}


return $config;
